// Importing dependencies
const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer')
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const jwt = require('express-jwt')
const jwksRsa = require('jwks-rsa')

// Database
const { startDatabase } = require('./database/db')
const { insertGuide, getGuides, getGuide, deleteGuide, updateGuide } = require('./database/guides')

//Path Uploads
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/')
    },
    
    filename: function(req, file, cb) {
        cb(null, file.originalname)
    }
})

const upload = multer({ storage: storage, fileFilter: (req, file, cb) => {
    if(!file.originalname.match(/\.(mp3|wav|midi)$/)) {
        return cb(new Error('Sólo archivos de audio son permitidos'), false)
    }
    cb(null, true)
}})

// App
const app = express()

// Usign dependencies
app.use(helmet())
app.use(express.json())
app.use(express.urlencoded({ extended:true }))
app.use(cors())
app.use(morgan('combined'))

/*const checkJwt = jwt({
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `https://cobri.auth0.com/.well-known/jwks.json`
    }),
    
    audience: 'https://gv-api',
    issuer: `https://cobri.auth0.com/`,
    algorithms: ['RS256']
});

app.use(checkJwt)*/


startDatabase();

app.get('/api/v1/guides/', async(req, res) => {
    res.send(await getGuides())
})

/*startDatabase().then(async (r) => {
    await getGuides()
})*/

app.get('/api/v1/guides/:id/', async(req, res) => {
    let r = await getGuide(req.params.id)
    !r ? res.status(404).send({
        success: false,
        message: 'No se encontraron registros'
    }) : res.status(200).send({
        success: true,
        message: 'Se encontraron registros',
        datos: r
    })
})

app.post('/api/v1/guides/', upload.any(), async(req, res) => {
    const newGuide = req.body.member
    await insertGuide(newGuide)
    res.send({message: 'Nuevo Miembro Agregado'})
    //let files = req.files == 0 ? false : true
    //res.send({status: files})
})

app.delete('/api/v1/guides/:id/', async(req, res) => {
    await deleteGuide(req.params.id)
    res.send({message: 'Se eliminó un miembro'})
})

app.put('/api/v1/guides/:id/', async (req, res) => {
    const guide = req.body
    await updateGuide(req.params.id, guide)
    res.send({message: 'Miembro actualizado'})
})

app.listen(3001, async() => {
    console.log('Server corriendo en el puerto 3001')
})