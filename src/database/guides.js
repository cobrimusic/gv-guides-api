const { getDatabase } = require('./db')
const { ObjectID } = require('mongodb')

const collectionName = 'guides'

async function insertGuide(m) {
    const db = await getDatabase()
    const { insertedId } = await db.collection(collectionName).insertOne({member: m})
    return insertedId
}

async function getGuides() {
    const db = await getDatabase()
    return await db.collection('guides').find({}).toArray()
}

async function getGuide(m_id) {
    const db = await getDatabase()
    return await db.collection(collectionName).find({
        _id: new ObjectID(m_id)
    }).toArray()
}

async function deleteGuide(m_id) {
    const db = await getDatabase()
    await db.collection(collectionName).deleteOne({
        _id: new ObjectID(m_id)
    })
}

async function updateGuide(m_id, m) {
    const db = await getDatabase()
    await db.collection(collectionName).findOneAndUpdate(
        { _id: new ObjectID(m_id) },
        { $set: { "guia": m } }
    );
}

module.exports = {
    insertGuide,
    getGuides,
    getGuide,
    deleteGuide,
    updateGuide
}