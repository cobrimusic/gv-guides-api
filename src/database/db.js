const { MongoMemoryServer } = require('mongodb-memory-server')
const { MongoClient } = require('mongodb')

let db = null

async function startDatabase() {
    const mongoDBURL = 'mongodb+srv://cobrimusic:cobrimusic@gv-guides-cluster-twaom.mongodb.net/test?retryWrites=true&w=majority'
    const client = new MongoClient(mongoDBURL, { useNewUrlParser: true })
    client.connect(err => {
        db = client.db('gv-guides-db')
    })

}

async function getDatabase() {
    if(!db) await startDatabase()
    return db
}

module.exports = {
    getDatabase,
    startDatabase,
}